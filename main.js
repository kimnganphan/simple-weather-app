$(document).ready(function () {
  "use strict";

  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  //gán sự kiện cho nút tìm kiếm
  $("#search-btn").on("click", onSearchBtnClick);
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  //hàm xử lý sự kiện ấn nút tìm kiếm địa điểm
  function onSearchBtnClick() {
    var location = $("#input-location").val().trim();
    callApiToGetWeatherData(location);
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //hàm gọi api để lấy dữ liệu thời tiết
  function callApiToGetWeatherData(location) {
    $.ajax({
      type: "GET",
      url: `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=7f13acf9bbe12107a055a2e1a3d233ed&units=metric&lang=en`,
      dataType: "json",
      success: function (responseObj) {
        //hiển thị thông tin lấy được từ api
        $(".container").show();
        handleFrontEndAfterReceiveData(responseObj);
      },
      error: function (err) {},
    });
  }
  // hàm hiển thị thông tin lấy được từ api
  function handleFrontEndAfterReceiveData(responseObj) {
    $("#location").html(responseObj.name);
    $("#temp").html(Math.round(responseObj.main.temp) + "&#176;C");
    //các trường hợp thời tiết
    var displayIcon = "";
    switch (responseObj.weather[0].main) {
      case "Rain":
        displayIcon = `<i class="fa-solid fa-cloud-showers-heavy"></i>`;
        break;
      case "Clouds":
        displayIcon = `<i class="fa-solid fa-cloud"></i>`;
        break;
      case "Drizzle":
        displayIcon = `<i class="fa-solid fa-cloud-rain"></i>`;
        break;
      case "Thunderstorm":
        displayIcon = `<i class="fa-solid fa-poo-storm"></i>`;
        break;
      case "Snow":
        displayIcon = `<i class="fa-solid fa-snowflake"></i>`;
        break;
      case "Clear":
        displayIcon = `<i class="fa-solid fa-sun"></i>`;
        break;
      case "Tornado":
        displayIcon = `<i class="fa-solid fa-tornado"></i>`;
        break;
      case "Smoke":
      case "Fog":
      case "Mist":
        displayIcon = `<i class="fa-solid fa-smog"></i>`;
        break;
      case "Dust":
      case "Sand":
      case "Ash":
        displayIcon = `<i class="fa-solid fa-sun-dust"></i>`;
        break;
      default:
        break;
    }
    $("#description")
      .html(responseObj.weather[0].main + " ")
      .append(displayIcon);

    $("#humidity").html(responseObj.main.humidity + "%");
    $("#wind").html(responseObj.wind.speed + "km");
    $("#feel-like").html(Math.round(responseObj.main.feels_like) + "&#176;C");
  }
});
